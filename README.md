
# Protfolio builder

Portfolio builder written with Flutter, build for web

### Setup

- Create your own local branch
- Pull
- Create ```flutter new project``` in folder with project
- Run ```flutter pub get``` command
- Modify ```consts.dart``` and ```data_containers.dart``` to get result you want
- Run ```flutter build web``` command to build project

---
### Author
- Yura Neroba
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/)