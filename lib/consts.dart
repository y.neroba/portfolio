import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';

bool isDarkMode = true;
double postSizeImage = 512;
double fullSizeImage = 1024;

void openLink(String link) async {
  try {
    if (!await canLaunchUrlString(link)) {
      print('Can\'t open url');
      return;
    }
    launchUrlString(
      link,
      mode: LaunchMode.externalApplication,
      webOnlyWindowName: '_blank',
    );
  } catch (e) {
    print(e);
  }
}

Image getImage(String imageName) {
  Image image;
  if (imageName.startsWith('http')) {
    image = Image.network(imageName);
  } else {
    image = Image.asset('assets/images/$imageName');
  }
  return image;
}

IconThemeData getIconTheme() {
  return isDarkMode
      ? const IconThemeData(color: Colors.white)
      : const IconThemeData(color: Colors.black);
}

// [GRAPHICAL INTERFACE] handler
Widget sh(double val) {
  return SizedBox(height: val);
}

Widget sw(double val) {
  return SizedBox(width: val);
}

double dh(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

double dw(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

TextStyle h2TextStyle() {
  return const TextStyle(
    fontSize: 24,
  );
}

TextStyle h1TextStyle() {
  return const TextStyle(
    fontSize: 18,
  );
}
