import 'package:flutter/material.dart';
import 'package:portfolio/data/data_container.dart';
import 'package:portfolio/posts/post_card.dart';
import 'package:portfolio/data/post_data.dart';

class AllPostsPage extends StatelessWidget {
  const AllPostsPage({super.key});

  @override
  Widget build(BuildContext context) {
    var posts = getPosts();
    var itemsCount = posts.length;

    return Scaffold(
      body: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          getBody(itemsCount, posts),
        ],
      ),
    );
  }

  Widget getBody(int itemsCount, List<PostData> posts) {
    return Expanded(
      child: Center(
        child: ListView.builder(
          itemCount: itemsCount,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.only(top: 10, bottom: 10),
              child: Container(
                alignment: Alignment.center,
                child: PostCard(
                  current: index,
                  postData: posts[index],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
