import 'package:awesome_icons/awesome_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:portfolio/consts.dart';
import 'package:portfolio/data/data_container.dart';
import 'package:portfolio/social_button.dart';

class AboutPage extends StatelessWidget {
  const AboutPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        iconTheme: getIconTheme(),
        title: Text(
          'About me',
          style: h2TextStyle(),
        ),
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
      ),
      body: getAboutPageBody(context),
    );
  }

  Widget getAboutPageBody(BuildContext context) {
    return Expanded(
      child: Center(
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: SizedBox(
                    width: 200,
                    height: 200,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: getImage(profileLink),
                    ),
                  ),
                ),
                const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Contact information'),
                    SocialButton(
                      name: 'Gitlab',
                      link: 'https://gitlab.com/y.neroba',
                      icon: FontAwesomeIcons.gitlab,
                    ),
                    SocialButton(
                      name: 'Telegram',
                      link: 'https://t.me/ur_neroba',
                      icon: Icons.telegram,
                    ),
                    SocialButton(
                      name: 'Linkedin',
                      link: 'https://linkedin.com/in/yura-neroba',
                      icon: FontAwesomeIcons.linkedin,
                    ),
                  ],
                ),
              ],
            ),
            const SocialButton(
                name: 'Download CV',
                link:
                    'https://drive.google.com/file/d/1NBvZSLeVbBcKK3KjhJVTWlOQocS1RKoB/view?usp=sharing',
                icon: FontAwesomeIcons.fileDownload),
            sh(10),
            const SizedBox(
              width: 325,
              child: Text(
                '''Hello my name is Yurii Neroba! I have been working in the game development industry for over five years. For the past year, I have worked as a Unity Technical Lead. My duties include ensuring timely product delivery, creating and implementing the company's technical strategy, and maintaining the overall quality of our work''',
                textAlign: TextAlign.justify,
              ),
            )
          ],
        ),
      ).animate().fadeIn(),
    );
  }
}
