import 'package:flutter/material.dart';
import 'package:portfolio/consts.dart';
import 'package:portfolio/data/post_data.dart';
import 'package:photo_view/photo_view.dart';

class PostPreview extends StatelessWidget {
  const PostPreview({super.key, required this.data});

  final PostData data;
  @override
  Widget build(BuildContext context) {
    var images = <Widget>[];
    var h = dh(context);
    var w = dw(context);
    var size = h > w ? w : h;
    if (size > fullSizeImage){
      size = fullSizeImage;
    }

    for (var i = 0; i < data.images.length; i++) {
      late ImageProvider imageProvider;
      var imageName = data.images[i];
      if (imageName.startsWith('http')) {
        imageProvider = NetworkImage(imageName);
      } else {
        var imagePath = 'assets/images/$imageName';
        imageProvider = AssetImage(imagePath);
      }
      var imageHolder = Hero(
        tag: imageName,
        child: SizedBox(
          height: size,
          width: size,
          child: PhotoView(
            tightMode: true,
            backgroundDecoration: BoxDecoration(
              color: (isDarkMode ? Colors.black : Colors.white),
            ),
            imageProvider: imageProvider,
          ),
        ),
      );
      images.add(imageHolder);
    }

    //var tags = data.getTagsAsWidgets(context);

    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        iconTheme: getIconTheme(),
        title: Hero(
          tag: data.name,
          child: Text(
            data.name,
            style: h2TextStyle(),
          ),
        ),
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
      ),
      body: Center(
        child: ListView(
          children: images,
        ),
      ),
    );
  }
}
