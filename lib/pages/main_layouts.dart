import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:portfolio/pages/all_posts_page.dart';
import 'package:portfolio/consts.dart';
import 'package:portfolio/data/data_container.dart';
import 'package:portfolio/pages/about_page.dart';
import 'package:portfolio/pages/all_products_page.dart';

class MainLayout extends StatefulWidget {
  const MainLayout({super.key, required this.onThemeChange});
  final Function onThemeChange;

  @override
  State<MainLayout> createState() => MainLayoutState();
}

class MainLayoutState extends State<MainLayout> {
  int selectedIndex = 0;
  String searchWord = '';
  final int postsIndex = 0;
  final int productsIndex = 1;
  final int cvIndex = 2;
  //AnimationController controller;
  @override
  void initState() {
    super.initState();
    //controller = AnimationController(vsync: TickerProvider())
  }

  @override
  Widget build(BuildContext context) {
    // Set<String> uniqTags = {};
    // for (var i = 0; i < posts.length; i++) {
    //   for (var j = 0; j < posts[i].tags.length; j++) {
    //     uniqTags.add(posts[i].tags[j]);
    //   }
    // }

    var w = dw(context);
    var h = dh(context);
    dynamic navigationBar;
    var sideBar = sh(0);
    if (w > h) {
      sideBar = getSideBar();
    } else {
      navigationBar = getNavBar();
    }
    return Scaffold(
      appBar: getAppBar(context),
      bottomNavigationBar: navigationBar,
      body: Row(
        children: [
          sideBar,
          getBody(),
        ],
      ),
    );
  }

  Icon getTabIcon(int tabIndex) {
    if (tabIndex == postsIndex) {
      if (selectedIndex == postsIndex) {
        return const Icon(Icons.photo_album);
      }
      return const Icon(Icons.photo_album_outlined);
    } else if (tabIndex == productsIndex) {
      if (selectedIndex == productsIndex) {
        return const Icon(Icons.work);
      }
      return const Icon(Icons.work_outline);
    } else {
      if (selectedIndex == cvIndex) {
        return const Icon(Icons.person);
      }
      return const Icon(Icons.person_outline);
    }
  }

  String getTabName(int tabIndex) {
    if (tabIndex == postsIndex) {
      return 'Posts';
    } else if (tabIndex == productsIndex) {
      return 'Projects';
    } else {
      return 'CV';
    }
  }

  NavigationRail getSideBar() {
    return NavigationRail(
      extended: true,
      destinations: [
        NavigationRailDestination(
          icon: getTabIcon(postsIndex).animate().slideX(),
          label: Text(getTabName(postsIndex)).animate().slideX(),
        ),
        NavigationRailDestination(
          icon: getTabIcon(productsIndex).animate(delay: const Duration(milliseconds: 100)).slideX(),
          label: Text(getTabName(productsIndex)).animate(delay: const Duration(milliseconds: 100)).slideX(),
        ),
        NavigationRailDestination(
          icon: getTabIcon(cvIndex).animate(delay: const Duration(milliseconds: 200)).slideX(),
          label: Text(getTabName(cvIndex)).animate(delay: const Duration(milliseconds: 200)).slideX(),
        ),
      ],
      selectedIndex: selectedIndex,
      onDestinationSelected: (value) => setState(() {
        selectedIndex = value;
      }),
    );
  }

  NavigationBar getNavBar() {
    return NavigationBar(
      destinations: [
        NavigationDestination(
          icon: getTabIcon(postsIndex),
          label: getTabName(postsIndex),
        ),
        NavigationDestination(
          icon: getTabIcon(productsIndex),
          label: getTabName(productsIndex),
        ),
        NavigationDestination(
          icon: getTabIcon(cvIndex),
          label: getTabName(cvIndex),
        ),
      ].animate(interval: const Duration(milliseconds: 100)).slideY(),
      onDestinationSelected: (value) => setState(() {
        selectedIndex = value;
      }),
      selectedIndex: selectedIndex,
    );
  }

  Widget getBody() {
    if (selectedIndex == cvIndex) {
      return const AboutPage().getAboutPageBody(context);
    } else if (selectedIndex == productsIndex) {
      var projects = getProjects();
      return const AllProductsPage().getBody(projects.length, projects);
    } else {
      var posts = getPosts();
      return const AllPostsPage().getBody(posts.length, posts);
    }
  }

  AppBar getAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Theme.of(context).colorScheme.inversePrimary,
      iconTheme: getIconTheme(),
      centerTitle: false,
      title: Text(
        'My porfolio',
        style: h2TextStyle(),
      ),
      actions: <Widget>[
        IconButton(
          icon: (isDarkMode
              ? const Icon(Icons.dark_mode)
              : const Icon(Icons.light_mode)),
          onPressed: () => {setState(() {isDarkMode = !isDarkMode; widget.onThemeChange();})},
        ).animate().shake(),
      ],
      leading: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            onTap: () => onProfilePress(context),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: getImage(profileLink),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void onProfilePress(BuildContext context) {
    setState(() {
      selectedIndex = cvIndex;
    });
  }
}
