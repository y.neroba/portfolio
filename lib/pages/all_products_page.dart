import 'package:flutter/material.dart';
import 'package:portfolio/data/data_container.dart';
import 'package:portfolio/data/project_data.dart';
import 'package:portfolio/posts/project_card.dart';

class AllProductsPage extends StatelessWidget {
  const AllProductsPage({super.key});

  @override
  Widget build(BuildContext context) {
    var posts = getProjects();
    var itemsCount = posts.length;

    return Scaffold(
      body: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          getBody(itemsCount, posts),
        ],
      ),
    );
  }

  Widget getBody(int itemsCount, List<ProjectData> posts) {
    return Expanded(
      child: Center(
        child: ListView.builder(
          itemCount: itemsCount,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.only(top: 10, bottom: 10),
              child: Container(
                alignment: Alignment.center,
                child: ProjectCard(
                  current: index,
                  projectData: posts[index],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
