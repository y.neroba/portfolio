import 'package:flutter/material.dart';
import 'package:portfolio/consts.dart';

class SocialButton extends StatelessWidget {
  const SocialButton(
      {super.key, required this.name, required this.link, required this.icon});
  final String name;
  final String link;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.5),
      child: RepaintBoundary(
        child: ElevatedButton.icon(
          //style: const ButtonStyle(padding: MaterialStatePropertyAll(EdgeInsets.all(5))),
          icon: Icon(icon),
          label: Text(
            name,
          ),
          onPressed: () => openLink(link),
        ),
      ),
    );
  }
}
