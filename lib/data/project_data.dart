import 'package:portfolio/data/post_data.dart';

class ProjectData extends PostData{
  ProjectData(super.name, super.images, super.tags);
  ProjectData.full(super.name, super.images, super.tags, {required this.links});
  
  Map<String, String> links = <String, String>{};
}