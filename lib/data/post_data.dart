import 'package:flutter/material.dart';

class PostData {
  String name;
  List<String> images;
  String description = '';
  List<String> tags;
  //DateTime creationTime;

  PostData(this.name, this.images, this.tags);
  PostData.withDescription(this.name, this.images, this.tags, this.description);

  List<Widget> getTagsAsWidgets({bool includeHashtag = true}){
    var widgets = <Widget>[];
    for (var i = 0; i < tags.length; i++) {
      var text = includeHashtag ? '#${tags[i]}' : tags[i];
      widgets.add(
        Padding(
          padding: const EdgeInsets.only(left: 5),
          child: RepaintBoundary(
            child: TextButton(
              onPressed: () => {},
              child: Text(
                text,
              ),
            ),
          ),
        ),
      );
    }
    return widgets;
  }
}