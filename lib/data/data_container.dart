import 'package:portfolio/data/post_data.dart';
import 'package:portfolio/data/project_data.dart';

String profileLink =
    'https://media.licdn.com/dms/image/D4D03AQF92sCpzH1CpQ/profile-displayphoto-shrink_800_800/0/1711531900031?e=1717027200&v=beta&t=nWQjPemSlQ0FBCDwSgGUzkacx08GUbhC0bJohoRKY2A';

List<PostData> getPosts() {
  return <PostData>[
    PostData('UNITY', [], []),
    PostData.withDescription(
        'Post effects example',
        [
          'https://pbs.twimg.com/media/FlP-UbwXkAATPH5?format=jpg&name=medium',
        ],
        ['3D', 'Unity', 'Post effects'],
        'All shaders ilustrated in scene were written by me, with Unity HLSL and Build-in pipeline'),
    PostData.withDescription(
        'Procedural grass',
        [
          'https://pbs.twimg.com/ext_tw_video_thumb/1634993296194797569/pu/img/VcdJNkqGzEQ9BiZD.jpg',
        ],
        ['3D', 'Unity', 'Shader'],
        'Geometry shader for grass with light intractions and simple physics'),
    PostData.withDescription(
        'Tessellation',
        [
          'https://pbs.twimg.com/ext_tw_video_thumb/1686624723394850817/pu/img/cyj9dthUrAxxU4fX.jpg',
        ],
        ['Unity', 'Shader'],
        'Base tessellation shader'),
    PostData.withDescription(
        'A-star pathfinder',
        [
          'https://pbs.twimg.com/tweet_video_thumb/Er-PtXRXcAMmgR5.jpg',
        ],
        ['Unity', 'Algorithms'],
        'My own implementation of A* pathfinder'),
    PostData(
      'Madelbrot set',
      [
        'https://pbs.twimg.com/media/Fg1d8zpXoAAjFXt?format=jpg&name=small',
        'https://pbs.twimg.com/media/Fg1cgzNXoAA8hXc?format=jpg&name=small',
        'https://pbs.twimg.com/media/Fg1elrKXEAAv-Y8?format=jpg&name=small',
        'https://pbs.twimg.com/media/Fg1fGXrXwAAmPFe?format=jpg&name=small'
      ],
      ['Shader', 'Math', 'Fractal'],
    ),
    PostData(
      'Fractals',
      [
        'https://pbs.twimg.com/media/FgvZvyNWYAITnTp?format=jpg&name=small',
        'https://pbs.twimg.com/media/FgvZ7YBX0AE-kDg?format=png&name=small',
        'https://pbs.twimg.com/media/FgvbSU2WQAAzc2z?format=png&name=small',
        'https://pbs.twimg.com/media/FgvbtAYWYAERdqJ?format=png&name=small'
      ],
      ['Shader', 'Math', 'Fractal'],
    ),
    PostData.withDescription(
        'Voronoi',
        ['https://pbs.twimg.com/media/FgQekdUWIAIH7Xi?format=jpg&name=medium'],
        ['Shader', 'Math'],
        'Voronoi texture, generated in realtime written in HLSL'),
    PostData(
      'Unity gizmos',
      [
        'https://pbs.twimg.com/media/FhcHqxOWQAAgHpp?format=png&name=small',
        'https://pbs.twimg.com/media/FhcIQtbX0AI1baq?format=png&name=small',
        'https://pbs.twimg.com/media/FhcIXoeXwAMWiA3?format=png&name=small',
        'https://pbs.twimg.com/media/FhcIXovX0AIcuiv?format=png&name=small'
      ],
      ['Unity', 'Math'],
    ),
    PostData.withDescription(
        'Wave function collapse',
        [
          'https://pbs.twimg.com/media/FgGZDFOXkAMM37k?format=png&name=small',
        ],
        ['Unity', 'Algorithms'],
        'My implementation of WFC algorithm'),
    PostData.withDescription('3D 🎨🗿', [], [], 'As a hobby I do 3D'),
    PostData(
      'Japanese dojo',
      [
        'https://pbs.twimg.com/media/GIKUVDCXAAIhObY?format=jpg&name=medium',
      ],
      ['3D', 'Blender', 'Low poly'],
    ),
    PostData(
      'Lonely house',
      [
        'https://pbs.twimg.com/media/GAYFfKAWUAAjzWe?format=jpg&name=medium',
      ],
      ['3D', 'Blender', 'Low poly'],
    ),
    PostData(
      'Last hope',
      [
        'https://pbs.twimg.com/media/FqURd43XoA4vPWA?format=png&name=900x900',
      ],
      ['3D', 'Blender', 'Low poly'],
    ),
    PostData(
      'Acient scene',
      [
        'https://pbs.twimg.com/media/Fq8SFNtWYAAbv7H?format=jpg&name=medium',
      ],
      ['3D', 'Blender', 'Low poly'],
    ),
    PostData(
      'Lighthouse',
      [
        'https://pbs.twimg.com/media/FT8FYlXWYAsAUlB?format=jpg&name=small',
      ],
      ['3D', 'Blender', 'Low poly'],
    ),
    PostData(
      'Clock Tower',
      [
        'https://pbs.twimg.com/media/FT8FbPyWAAA_3p5?format=jpg&name=small',
      ],
      ['3D', 'Blender', 'Low poly'],
    ),
  ];
}

List<ProjectData> getProjects() {
  return <ProjectData>[
    ProjectData.full(
      'My Fantasy',
      [
        'https://play-lh.googleusercontent.com/dbc03LYr-yytxhAtfG97rQ6RlOch7dlrwpMXowqgpNhc-zyRx57FtvswvKWlG2x0Iw=w256-h960-rw'
      ],
      ['Unity', 'Visual novel', '2d'],
      links: Map<String, String>.unmodifiable(
        {
          'iOS':
              'https://apps.apple.com/ms/app/my-fantasy-choose-your-story/id1491717191',
          'Android':
              'https://play.google.com/store/apps/details?id=gmem.episode&hl=en',
        },
      ),
    ),
    ProjectData.full(
      'Blushed',
      [
        'https://play-lh.googleusercontent.com/bSkk7IwPoWHf9LwoXK9wmmyghOkl_pGTeIsmgdbvKN1C6_c-KpgSE7mt4PtLwTvdOqm_=w256-h256'
      ],
      ['Unity', 'Visual novel', '2d'],
      links: Map<String, String>.unmodifiable(
        {
          'iOS':
              'https://apps.apple.com/us/app/blushed-romance-choices/id6466787923',
          'Android':
              'https://play.google.com/store/apps/details?id=com.sonidorico.blushed&hl=en',
        },
      ),
    ),
    ProjectData.full(
      'My portfolio',
      ['https://gitlab.com/uploads/-/system/project/avatar/56171472/image_2024-04-01_131756764.png?width=180'],
      ['Flutter', 'Web'],
      links: Map<String, String>.unmodifiable(
        {
          'Gitlab': 'https://gitlab.com/y.neroba/portfolio',
        },
      ),
    ),
    ProjectData.full(
      'Archer`s tale',
      [
        'https://play-lh.googleusercontent.com/zGU1p4xIV9GYvv6TpEnfgOAdk50tKa1i2i9adBJXloGXHzuJ1n7vVpDhIUAMTrGiyeLV=s256-rw'
      ],
      ['Unity', 'ARPG', 'Casual', '3D'],
      links: Map<String, String>.unmodifiable(
        {
          'Android':
              'https://play.google.com/store/apps/details?id=com.babil.archers.tale.bow.hunter.battle.action',
          'Youtube': 'https://www.youtube.com/watch?v=0EeTccu2i0U',
        },
      ),
    ),
    ProjectData.full(
      'Cartoon2048',
      [
        'https://play-lh.googleusercontent.com/VC6TjXw9CfFI2sITOY0Wvs7Yu4TZW_oovclEDMRI17wiFkCCjsqalhrEeJrZtgBKDFU=w256-h960-rw'
      ],
      ['Unity', 'Puzzle', '3D'],
      links: Map<String, String>.unmodifiable(
        {
          'Android':
              'https://play.google.com/store/apps/details?id=com.JuicyJam.Cartoon2048',
        },
      ),
    ),
    ProjectData.full(
      'Homesteads',
      [
        'https://play-lh.googleusercontent.com/oDAu0VnIxnERXFE8gElx0tYW2T7ncZFJbV06rjas-YfXNHvdJUSOPH8JWa19Q41jCw=s256-rw'
      ],
      ['Unity', 'Farm', '2D'],
      links: Map<String, String>.unmodifiable(
        {
          'Android':
              'https://play.google.com/store/apps/details?id=com.Enixan.Homestead&hl=en&gl=US',
          'iOS':
              'https://apps.apple.com/us/app/homesteads-dream-farm-town/id1538149075',
        },
      ),
    ),
  ];
}
