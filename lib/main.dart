import 'package:flutter/material.dart';
import 'package:portfolio/consts.dart';
import 'package:portfolio/pages/main_layouts.dart';

void main() {
  // init services
  // prepare some data
  //
  // show widgets
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late ColorScheme colors;
  @override
  Widget build(BuildContext context) {
    colors = isDarkMode ? const ColorScheme.dark() : const ColorScheme.light();
    return MaterialApp(
      title: 'Portfolio',
      theme: ThemeData(
        colorScheme: colors,
        useMaterial3: true,
      ),
      home: MainLayout(
        onThemeChange: () => {
          setState(() => ()),
        },
      ),
    );
  }
}
