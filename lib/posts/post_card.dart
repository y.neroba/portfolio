import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:portfolio/consts.dart';
import 'package:portfolio/data/post_data.dart';
import 'package:portfolio/pages/post_preview.dart';

class PostCard extends StatelessWidget {
  const PostCard({super.key, required this.current, required this.postData});
  final int current;
  final PostData postData;

  @override
  Widget build(BuildContext context) {
    var postName = postData.name;
    var postDescription = postData.description;

    var screenWidth = dw(context);
    var w = screenWidth * .9;

    if (w > postSizeImage) {
      w = postSizeImage;
    }
    var imagesInPost = postData.images.length;
    var imageName = '';
    List<Widget> images = [];
    for (var i = 0; i < imagesInPost; i++) {
      imageName = postData.images[i];
      Image image;
      if (imageName.startsWith('http')) {
        image = Image.network(
          imageName,
        );
      } else {
        image = Image.asset(
          'assets/images/$imageName',
        );
      }
      late BorderRadius border;
      if (imagesInPost == 1) {
        border = BorderRadius.circular(8);
      } else if (i == 0) {
        border = const BorderRadius.only(topLeft: Radius.circular(8));
      } else if (i == 1) {
        border = const BorderRadius.only(topRight: Radius.circular(8));
      } else if (i == 2) {
        border = const BorderRadius.only(bottomLeft: Radius.circular(8));
      } else if (i == 3) {
        border = const BorderRadius.only(bottomRight: Radius.circular(8));
      } else {
        border = BorderRadius.circular(8);
      }
      var imageHolder = Hero(
        tag: imageName,
        child: ClipRRect(
          borderRadius: border,
          child: FittedBox(
            fit: BoxFit.fill,
            child: image,
          ),
        ),
      );

      images.add(imageHolder);
    }
    var tags = postData.getTagsAsWidgets();
    var descriptionExist = postDescription.isNotEmpty;

    return Card(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: RepaintBoundary(
              child: SizedBox(
                height: imagesInPost > 0 ? w : 10,
                width: w,
                child: GestureDetector(
                  onTap: () => onImagePress(context, postData),
                  child: GridView.count(
                    physics: const NeverScrollableScrollPhysics(),
                    crossAxisCount: images.length > 1 ? 2 : 1,
                    children: images,
                  ),
                ),
              ),
            ),
          ).animate().fadeIn(),
          Text(
            postName,
            style: h2TextStyle(),
          ),
          SizedBox(
            width: w,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: tags,
            ),
          ),
          sh(10),
          if (descriptionExist) SizedBox(width: w * .9, child: Text(postDescription, textAlign: TextAlign.center,)),
          if (descriptionExist) sh(10),
        ],
      ),
    );
  }

  void onImagePress(BuildContext context, PostData postData) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => PostPreview(data: postData),
      ),
    );
  }
}
