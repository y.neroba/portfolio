import 'package:flutter/material.dart';
import 'package:portfolio/consts.dart';
import 'package:portfolio/data/post_data.dart';
import 'package:portfolio/data/project_data.dart';
import 'package:portfolio/social_button.dart';

class ProjectCard extends StatelessWidget {
  const ProjectCard(
      {super.key, required this.current, required this.projectData});
  final int current;
  final ProjectData projectData;

  @override
  Widget build(BuildContext context) {
    var postName = projectData.name;
    var imageName = projectData.images;
    var image = getImage(imageName[0]);

    var screenWidth = dw(context);
    var cardWidth = screenWidth * .9;
    var w = 150.0;

    if (cardWidth > postSizeImage) {
      cardWidth = postSizeImage;
    }
    List<Widget> links = <Widget>[];

    projectData.links.forEach((key, value) {
      links.add(
        SocialButton(
          name: key,
          link: value,
          icon: Icons.open_in_new,
        ),
      );
    });
    var tags = projectData.getTagsAsWidgets(includeHashtag: false);
    var infoWidth = cardWidth - w - 10; // 10 - image paddings
    return Card(
      child: SizedBox(
        width: cardWidth,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: SizedBox(
                height: w,
                width: w,
                child: Hero(
                  tag: imageName,
                  child: GestureDetector(
                    onTap: () => onImagePress(context, projectData),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: image,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 160,
              width: infoWidth,
              child: Column(
                children: [
                  sh(5),
                  Text(
                    postName,
                    style: h1TextStyle(),
                  ),
                  SizedBox(
                    width: infoWidth,
                    child: Wrap(
                      children: links,
                    ),
                  ),
                  SizedBox(
                    width: infoWidth,
                    child: Wrap(
                      children: tags,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void onImagePress(BuildContext context, PostData postData) {}
}
